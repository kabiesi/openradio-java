package org.m4ndr01d.radio;
import android.app.*;
import android.content.*;
import android.content.res.*;
import android.graphics.*;
import android.media.*;
import android.media.session.*;
import android.os.*;
import android.util.*;

public class Helper {
	private Helper(){}
	public static void sendMediaNotification(Service ctx,String title,Object session){
		Notification.Builder build = new Notification.Builder(ctx);
		PendingIntent p1 = create(ctx,ACTION_PAUSE), p2 = create(ctx,ACTION_RECORD);
		build.addAction(android.R.drawable.ic_media_pause,"Pause",p1);
		build.addAction(android.R.drawable.ic_btn_speak_now,"Record",p2);
		if(Build.VERSION.SDK_INT >= 21){
			Notification.MediaStyle ms = new Notification.MediaStyle();
			ms.setMediaSession(((MediaSession)session).getSessionToken());
			ms.setShowActionsInCompactView(0);
			build.setStyle(ms);
		}
		if(Build.VERSION.SDK_INT >= 26){
			NotificationManager nm = (NotificationManager) ctx.getSystemService(ctx.NOTIFICATION_SERVICE);
			NotificationChannel nc = new NotificationChannel(ACTION_PAUSE,ctx.getClass().getSimpleName(),NotificationManager.IMPORTANCE_LOW);
			nm.createNotificationChannel(nc);
			build.setChannelId(nc.getId());
		}
		build.setContentTitle(title);
		build.setShowWhen(false);
		build.setLargeIcon(BitmapFactory.decodeResource(ctx.getResources(),R.drawable.ic_launcher));
		build.setSmallIcon(android.R.drawable.ic_media_play);
		Notification n = build.build();
		n.flags |= Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;
		ctx.startForeground(ACTION_PAUSE.hashCode(),n);
	}
	
	private static final PendingIntent create(Context ctx, String action){
		return PendingIntent.getBroadcast(ctx, 0, new Intent(action), 0);
	}
	
	public static final String ACTION_PAUSE = "org.m4ndr01d.radio.PAUSE",
								ACTION_RECORD = "org.m4ndr01d.radio.RECORD";
	
	public static final MediaMetadata getMetadata(Resources r, String title){
		MediaMetadata.Builder build = new MediaMetadata.Builder();
		build.putString(MediaMetadata.METADATA_KEY_TITLE,title);
		build.putBitmap(MediaMetadata.METADATA_KEY_ALBUM_ART,BitmapFactory.decodeResource(r,R.drawable.ic_launcher));
		return build.build();
	}
	
	public static float dp(float br){
		return Resources.getSystem().getDisplayMetrics().density * br;
	}
	
	public static float mp(float br){
		DisplayMetrics m = Resources.getSystem().getDisplayMetrics();
		float x = m.widthPixels > m.heightPixels ? m.heightPixels : m.widthPixels;
		x /= 100f;
		return x * br;
	}
	
	public static int getBitmapColor(Bitmap bitmap){
		if (bitmap == null) return 0xFF000000;
		bitmap = bitmap.createScaledBitmap(bitmap,64,64,false);
		int width = bitmap.getWidth(),height = bitmap.getHeight();
		int pixels[] = new int[width * height];
		bitmap.getPixels(pixels, 0, width, 0, 0, width, height);
		int color,count = 0,r = 0,g = 0,b = 0,a = 0;
		for(int i = 0;i < pixels.length;i++){
			color = pixels[i];
			a = Color.alpha(color);
			if(a > 0){
				color = (a < 255) ? Color.rgb(Color.red(color),Color.green(color),Color.blue(color)) : color;
				r += Color.red(color);
				g += Color.green(color);
				b += Color.blue(color);
				count++;
			}
		}
		if(r == g && g == b && r == 0){
			count = 1;
		}
		r /= count;
		g /= count;
		b /= count;
		r = (r << 16) & 0x00FF0000;
		g = (g << 8) & 0x0000FF00;
		b = b & 0x000000FF;
		color = 0xFF000000 | r | g | b;
		return color;
	}
}
