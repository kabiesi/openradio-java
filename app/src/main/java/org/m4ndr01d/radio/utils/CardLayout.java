package org.m4ndr01d.radio.utils;

import android.content.*;
import android.widget.*;
import android.graphics.drawable.*;
import android.view.*;
import android.util.*;
import android.graphics.*;
import android.content.res.*;

import static org.m4ndr01d.radio.Helper.*;
import org.m4ndr01d.radio.*;

public class CardLayout extends LinearLayout {
	
	private final float rad = dp(8);
	private TextView tv = null;
	private ImageView iv = null;
	
	private void setCardBackgroundColor(int color){
		GradientDrawable gd = null;
		if(getBackground() != null && getBackground() instanceof GradientDrawable){
			gd = (GradientDrawable) getBackground();
		} else {
			gd = new GradientDrawable();
			gd.setCornerRadius(rad);
			setBackgroundDrawable(gd);
		}
		gd.setColor(color);
		iv.setTag(color);
	}
	
	public void setCard(String text, Bitmap bmp){
		tv.setText(text);
		iv.setImageBitmap(bmp);
		setCardBackgroundColor(Helper.getBitmapColor(bmp));
	}
	
	public void setCard(String text, int drwRes){
		setCard(text,BitmapFactory.decodeResource(getResources(),drwRes));
	}
	
	public CardLayout(Context c){
		this(c,null);
	}

	public CardLayout(Context c, AttributeSet a){
		super(c,a);
		tv = new TextView(c);
		tv.setLayoutParams(new LayoutParams(-1,-1,1));
		tv.setTypeface(Typeface.DEFAULT_BOLD);
		tv.setGravity(Gravity.CENTER);
		tv.setTextColor(0xFFDEDEDE);
		tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP,16);
		iv = new ImageView(c);
		iv.setLayoutParams(new LayoutParams(-1,-1,1));
		iv.setAdjustViewBounds(true);
		int i = (int) Helper.mp(2);
		iv.setPadding(i,i,i,i);
		addView(tv);
		addView(iv);
		setCardBackgroundColor(0);
	}

	public CardLayout(Context c, AttributeSet a, int da){
		this(c,a);
	}

	public CardLayout(Context c, AttributeSet a, int da, int dr){
		this(c,a);
	}
}
